/**
 * Представление узла в бинарном дереве.
 * @param value
 * @constructor
 */
var Node = function (value) {

    /**
     * Текущее значение узла.
     */
    this.value = value;

    /**
     * Родительский узел.
     * @type {undefined}
     */
    this.parent = undefined;

    /**
     * Дочерний правый узел.
     * @type {undefined}
     */
    this.right = undefined;

    /**
     * Дочерний левый узел.
     * @type {undefined}
     */
    this.left = undefined;

    /**
     * Устанавливает значение для узла.
     * @param value
     */
    this.setValue = function(value) {
        this.value = value;
    };

    /**
     * Возвращает значение для узла.
     * @returns {*}
     */
    this.getValue = function() {
        return this.value;
    };

    /**
     * Возвращает True если текущий узел является правым дочерним узлом родительского узла.
     * @returns {boolean}
     */
    this.amIRightChild = function() {
        return this.parent.right === this;
    };

    /**
     * Возвращает True если текущий узел является левым дочерним узлом родительского узла.
     * @returns {boolean}
     */
    this.amILeftChild = function() {
        return this.parent.left === this;
    };

    /**
     * Узел удаляет сам себя из дерева.
     */
    this.delete = function() {
        // if the node has no both children
        if (this.hasRightChild() === false && this.hasLeftChild() === false) {
            if (this.amILeftChild()) {
                this.getParent().setLeft(undefined);
            } else {
                this.getParent().setRight(undefined);
            }

            return;
        }

        // if the node only has right child
        if (this.hasRightChild() && this.hasLeftChild() === false) {
            if (this.amILeftChild()) {
                this.getParent().setLeft(this.getRightChild());
            } else {
                this.getParent().setRight(this.getRightChild());
            }

            this.getRightChild().setParent(
                this.getParent()
            );

            this.setParent(undefined);

            return;
        }

        // 2
        if (this.hasLeftChild() && this.hasRightChild() === false) {
            if (this.amILeftChild()) {
                this.getParent().setLeft(this.getLeftChild());
            } else {
                this.getParent().setRight(this.getLeftChild());
            }

            this.getLeftChild().setParent(
                this.getParent()
            );

            this.setParent(undefined);

            return;
        }

        // 3

        if (this.hasLeftChild() && this.hasRightChild()) {
            var min = this.getRightChild().min();

            this.setValue(min.getValue());

            min.delete();
        }
    };

    /**
     * Устанавливает родительский узел для текущего узла.
     * @param node
     * @returns {Node}
     */
    this.setParent = function(node) {
        this.parent = node;

        return this;
    };

    /**
     * Возвращает родительский узел для текущего узла.
     * @returns {undefined}
     */
    this.getParent = function() {
        return this.parent;
    };

    /**
     * Возвращает True если текущий узел имеет правого потомка.
     * @returns {boolean}
     */
    this.hasRightChild = function() {
        return this.right !== undefined;
    };

    /**
     * Возвращает True если текущий узел имеет левого потомка.
     * @returns {boolean}
     */
    this.hasLeftChild = function() {
        return this.left !== undefined;
    };

    /**
     * Возвращает левого потомка.
     * @returns {undefined}
     */
    this.getLeftChild = function() {
        return this.left;
    };

    /**
     * Возвращает правого потомка.
     * @returns {undefined}
     */
    this.getRightChild = function() {
        return this.right;
    };

    /**
     * Устанавливает правого потомка для текущего узла.
     * @param right
     * @returns {Node}
     */
    this.setRight = function (right) {
        this.right = right;

        return this;
    };

    /**
     * Устанавливает левого потомка для текущего узла.
     * @param left
     * @returns {Node}
     */
    this.setLeft = function (left) {
        this.left = left;

        return this;
    };

    /**
     * Возвращает самого левого потомка начиная от текущего узла.
     * @returns {Node}
     */
    this.min = function() {
        var node = this;

        while (node.left) {
            if (node.left === undefined) {
                break;
            }

            node = node.left;
        }

        return node;
    };
};

/**
 * Представление бинарного дерева.
 * @constructor
 */
var BinaryTree = function() {

    /**
     * Указатель на самый первый узел.
     * @type {undefined}
     */
    this.head = undefined;

    /**
     * Длинна бинарного дерева.
     * @type {number}
     */
    this.length = 0;

    /**
     * Кладет элемент в дерево рекурсивно.
     * @param value
     * @param node
     */
    var pushRecursively = function(value, node) {
        if (node.getValue() > value) {
            if (node.hasLeftChild()) {
                pushRecursively(value, node.getLeftChild());
            } else {
                node.setLeft(
                    (new Node(value)).setParent(node)
                );
            }
        } else {
            if (node.hasRightChild()) {
                pushRecursively(value, node.getRightChild());
            } else {
                node.setRight(
                    (new Node(value)).setParent(node)
                );
            }
        }
    };

    /**
     * Симетрический обход рекурсивно.
     * @param node
     * @param callback
     */
    var inOrderRecursively = function(node, callback) {
        if (node.hasLeftChild()) {
            inOrderRecursively(node.getLeftChild(), callback)
        }

        callback(node.getValue());

        if (node.hasRightChild()) {
            inOrderRecursively(node.getRightChild(), callback);
        }
    };

    /**
     * Обратный обход рекурсивно.
     * @param node
     * @param callback
     */
    var postOrderRecursively = function(node, callback) {
        if (node.hasLeftChild()) {
            postOrderRecursively(node.getLeftChild(), callback)
        }

        if (node.hasRightChild()) {
            postOrderRecursively(node.getRightChild(), callback);
        }

        callback(node.getValue());
    };

    /**
     * Прямой обход рекурсивно.
     * @param node
     * @param callback
     */
    var preOrderRecursively = function(node, callback) {
        callback(node.getValue());

        if (node.hasLeftChild()) {
            preOrderRecursively(node.getLeftChild(), callback)
        }

        if (node.hasRightChild()) {
            preOrderRecursively(node.getRightChild(), callback);
        }
    };

    /**
     * Кладет элемент в бинарное дерево.
     * @param value
     */
    this.push = function(value) {

        if (this.head === undefined) {
            this.head = new Node(value);
            this.length++;
        } else {
            pushRecursively(value, this.head);
            this.length++;
        }
    };

    /**
     * Возвращает True если значение содержиться в бинарном дереве.
     * @param value
     * @returns {boolean}
     */
    this.contains = function(value) {
        var node = this.head;

        while (true) {

            if (node === undefined) {
                return false;
            }

            if (value === node.value) {
                return true;
            }

            if (value < node.value) {
                node = node.left;
            } else {
                node = node.right;
            }
        }
    };

    /**
     * Очищает бинарное дерево.
     */
    this.clear = function() {
        this.length = 0;
        this.head = undefined;
    };

    /**
     * Возвращает размер бинарного дерева.
     * @returns {number}
     */
    this.size = function() {
        return this.length;
    };

    /**
     * Удаляет узел с указанным значением.
     * @param value
     * @returns {boolean}
     */
    this.delete = function(value) {
        var node = this.head;

        while (true) {

            if (node === undefined) {
                return false;
            }

            if (value === node.value) {
                node.delete();

                return true;
            }

            if (value < node.value) {
                node = node.left;
            } else {
                node = node.right;
            }
        }
    };

    /**
     * Симетрический обход дерева.
     * @param callback
     */
    this.inOrder = function(callback) {
        if (this.head === undefined) {
            return;
        }

        inOrderRecursively(this.head, callback);
    };

    /**
     * Обратный обход дерева.
     * @param callback
     */
    this.postOrder = function(callback) {
        if (this.head === undefined) {
            return;
        }

        postOrderRecursively(this.head, callback);
    };

    /**
     * Прямой обход дерева.
     * @param callback
     */
    this.preOrder = function(callback) {
        if (this.head === undefined) {
            return;
        }

        preOrderRecursively(this.head, callback);
    };
};

var bt = new BinaryTree();

bt.push(5);
bt.push(2);
bt.push(11);
bt.push(1);
bt.push(4);
bt.push(7);
bt.push(23);
bt.push(16);
bt.push(34);

bt.preOrder(console.log);
bt.inOrder(console.log);
bt.postOrder(console.log);
