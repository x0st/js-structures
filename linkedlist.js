/**
 * Представление узла в связном списке.
 *
 * @param value
 * @constructor
 */
var Node = function (value) {

    /**
     * Значение узла.
     */
    this.value = value;

    /**
     * Указатель на следующий узел.
     * @type {undefined}
     */
    this.next = undefined;

    /**
     * Устаналивает указатель на следующий узел.
     * @param next
     */
    this.setNext = function (next) {
        this.next = next;
    }
};

/**
 * Представление связного списка.
 * @constructor
 */
var LinkedList = function () {

    /**
     * Длинна связного списка.
     * @type {number}
     */
    this.length = 0;

    /**
     * Указатель на первый элемент.
     * @type {undefined}
     */
    this.head = undefined;

    /**
     * Указатель на последний элемент.
     * @type {undefined}
     */
    this.tail = undefined;

    /**
     * Кладет элемент в конец связного списка.
     * @param value
     */
    this.push = function(value) {
        this.length++;

        var item = new Node(value);

        if (this.head === undefined) {
            this.head = item;
            this.tail = item;
            return;
        }

        this.tail.setNext(item);
        this.tail = item;
    };

    /**
     * Возвращает размер связного списка.
     * @returns {number}
     */
    this.size = function() {
        return this.length;
    };

    /**
     * Удаляет узел с указанным значением.
     * @param value
     * @returns {boolean}
     */
    this.delete = function(value) {
        if (this.size() === 0) {
            return;
        }

        var prevNode = this.head;
        var node = this.head;

        if (node.value === value) {
            this.head = this.head.next;
            this.length--;
            return;
        }

        while (node !== undefined) {
            if (node.value === value) {
                prevNode.next = node.next;
                this.length--;
                return true;
            }

            prevNode = node;
            node = node.next;
        }

        return false;
    };

    /**
     * Проверяет, имеется ли указаное значение в связном списке.
     * @param value
     * @returns {boolean}
     */
    this.contains = function(value) {
        var node = this.head;

        while (node !== undefined) {
            if (node.value === value) {
                return true;
            }

            node = node.next;
        }

        return false;
    };

    /**
     * Очищает связный список.
     */
    this.clear = function() {
        this.length = 0;
        this.head = undefined;
        this.tail = undefined;
    };

    /**
     * Возвращает значение первого узла.
     * @returns {*}
     */
    this.getFirst = function() {
        return this.head === undefined ? undefined : this.head.value;
    };

    /**
     * Возвращает значение последнего узла.
     * @returns {*}
     */
    this.getLast = function() {
        return this.tail === undefined ? undefined : this.tail.value;
    };

    /**
     * Удаляет первый узел.
     */
    this.removeFirst = function() {
        if (this.size() === 0) {
            return;
        }

        if (this.size() === 1) {
            this.head = undefined;
            this.tail = undefined;
            this.length--;
            return;
        }

        this.length--;
        this.head = this.head.next;
    };

    /**
     * Итерирует связный список и применяет фк-цию для каждого значения узла в списке.
     * @param callback
     */
    this.iterate = function(callback) {
        var node = this.head;

        while (node !== undefined) {
            callback(node.value);

            node = node.next;
        }
    };
};

var ll = new LinkedList();

ll.push(1);
ll.push(2);
ll.push(3);

ll.iterate(console.log);
