/**
 * Представление хэш таблицы.
 *
 * @constructor
 */
var HashMap = function() {

    /**
     * Массив, где хранятся все значения.
     * @type {Array}
     */
    var array = [];

    /**
     * Хэш-фкция для получения численного представления строки.
     * @param value
     * @returns {number}
     */
    var applyHashFunction = function(value) {
        var hash = 0, i, chr;

        if (value.length === 0) return hash;

        for (i = 0; i < value.length; i++) {
            chr   = value.charCodeAt(i);
            hash  = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }

        return hash;
    };

    /**
     * Кладет значение в хэш мап.
     * @param key
     * @param value
     */
    this.put = function(key, value) {
        array[applyHashFunction(key)] = value;
    };

    /**
     * Проверяет, существует ли указанный ключ в хэш таблице.
     * @param key
     * @returns {boolean}
     */
    this.hasKey = function(key) {
        return array[applyHashFunction(key)] !== undefined;
    };

    /**
     * Возвращает значение по ключу.
     * @param key
     * @returns {*}
     */
    this.get = function(key) {
        return array[applyHashFunction(key)];
    };

    /**
     * Удаляет значение по ключу.
     * @param key
     */
    this.delete = function(key) {
        if (this.hasKey(key)) {
            delete array[applyHashFunction(key)];
        }
    };

    /**
     * Возвращает значение и сразу же удаляет его.
     * @param key
     * @returns {*}
     */
    this.pop = function(key) {
        var result = this.get(key);
        this.delete(key);

        return result;
    }
};

var hm = new HashMap();

hm.put('key', 'value');

console.log(hm.pop('value'));
