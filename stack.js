var stack = {

    /**
     * Указывает на индекс в массиве.
     */
    iterator: 0,

    /**
     * Массив элементов стэка.
     */
    list: [],

    /**
     * Кладет элемент в стэк.
     *
     * @param item
     */
    push: function(item) {
        this.list.push(item);
        this.iterator++;
    },

    /**
     * Извлекает элемент из стека.
     *
     * @returns {*}
     */
    pop: function() {
        this.iterator--;
        var item = this.list[this.iterator];
        this.list.splice(this.iterator, 1);
        return item;
    }
};

stack.push(1);
stack.push(2);
stack.push(3);
console.log(stack.pop());
stack.push(4);
console.log(stack.pop());
console.log(stack.pop());
stack.push(5);
console.log(stack.pop());
console.log(stack.pop());
