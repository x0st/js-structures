var queue = {

    /**
     * Массив элементов очереди.
     */
    queue: [],

    /**
     * Кладет элемент в очередь.
     *
     * @param item
     */
    push: function(item) {
        this.queue.push(item);
    },

    /**
     * Достает элемент из очереди.
     *
     * @returns {*}
     */
    pop: function() {
        return this.queue.splice(0, 1)[0];
    }
};

queue.push(1);
queue.push(2);

console.log(queue.pop());

queue.push(3);

console.log(queue.pop());
queue.push(4);
queue.push(5);
console.log(queue.pop());
